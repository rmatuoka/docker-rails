# docker-rails
Depois de baixar o projeto, substitua "sample-app" pelo nome do seu projeto.

Para rodar comandos do rails dentro do docker utilizar:

docker-compose run web RAILS_COMMAND

Ex: docker-compose run web rails new app --api


# Exemplo de database.yml

    default: &default
      adapter: mysql2
      encoding: utf8
      pool: 5
      database: <%= ENV['DB_NAME'] %>
      username: <%= ENV['DB_USER'] %>
      password: <%= ENV['DB_PASSWORD'] %>
      host: <%= ENV['DB_HOST'] %>

    development:
      <<: *default


    test:
      <<: *default


    production:
      <<: *default